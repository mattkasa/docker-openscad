FROM alpine:latest AS builder

RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/testing \
    alpine-sdk \
    cmake \
    qt5-qtbase-dev \
    qt5-qtmultimedia-dev \
    qt5-qtsvg-dev \
    qscintilla-dev \
    cgal-dev \
    gmp-dev \
    boost-dev \
    opencsg-dev \
    glew-dev \
    eigen-dev \
    glib-dev \
    fontconfig-dev \
    freetype-dev \
    harfbuzz-dev \
    libzip-dev \
    bison \
    flex-dev \
    double-conversion-dev \
    lib3mf-dev \
    libxml2-dev \
    cairo-dev \
    imagemagick

RUN ln -s /usr/lib/qt5/bin/qmake /usr/bin/qmake

RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/v3.10/main \
    mpfr-dev

WORKDIR /build

RUN git clone --depth 1 https://github.com/openscad/openscad.git \
 && cd openscad \
 && git submodule update --init \
 && mkdir build \
 && cd build \
 && CMAKE_PREFIX_PATH=/usr/share/cmake/Modules/ cmake -DHEADLESS=ON .. \
 && make

FROM alpine:latest

RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/testing \
    coreutils \
    qt5-qtbase \
    qt5-qtmultimedia \
    qt5-qtsvg \
    qscintilla \
    cgal \
    gmp \
    boost \
    opencsg \
    glew \
    glu \
    eigen \
    glib \
    fontconfig \
    freetype \
    harfbuzz \
    libzip \
    bison \
    flex \
    double-conversion \
    lib3mf \
    libxml2 \
    cairo \
    imagemagick \
    py3-pip

RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/v3.10/main \
    mpfr

RUN pip3 install sca2d

COPY --from=builder /build/openscad/build/openscad /usr/bin/openscad

ENTRYPOINT ["/usr/bin/env"]

